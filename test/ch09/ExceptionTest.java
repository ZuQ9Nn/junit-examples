package ch09;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

public class ExceptionTest {

	@Test(expected = IllegalArgumentException.class)
	public void test() {
		throw new IllegalArgumentException();
	}

	@Ignore("例外が発生しないため、テストは失敗する")
	@Test
	public void 例外の発生とメッセージを検証する標準的なテスト() throws Exception {
		try {
			throwNewIllegalArgumetException();
			fail("例外が発生しない");
		} catch (IllegalArgumentException e) {
			assertThat(e.getMessage(), is("argument is null."));
		}
	}
	private void throwNewIllegalArgumetException() {
		
	}
}
