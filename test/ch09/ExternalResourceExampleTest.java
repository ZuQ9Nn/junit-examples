package ch09;

import static org.junit.Assert.fail;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

public class ExternalResourceExampleTest {

	@Rule
	public ServerResource resourece = new ServerResource();
	
	static class ServerResource extends ExternalResource {
	
		Server server;
		
		@Override
		protected void before() throws Throwable {
			server = new Server(8080);
			server.start();
		}
		
		@Override
		protected void after() {
			server.shutdown();
		}
	}
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
