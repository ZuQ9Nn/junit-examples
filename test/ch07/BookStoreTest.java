package ch07;

import static ch07.BookStoreDeclarativeTestHelper.Bookオブジェクトの生成_MarinFowlerのRefactoring;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class BookStoreTest {

	@Test
	public void getTotalPrice() throws Exception {
		
		BookStore sut = new BookStore();
		Book book = Bookオブジェクトの生成_MarinFowlerのRefactoring();
		sut.addToCart(book, 1);
		assertThat(sut.getTotalPrice(), is(4500));
	}

	@Test
	public void get_0() throws Exception {
		
		BookStore sut = new BookStore();
		Book book =  Bookオブジェクトの生成_MarinFowlerのRefactoring();
		sut.addToCart(book, 1);
		assertThat(sut.get(0), is(sameInstance(book)));
	}
}
