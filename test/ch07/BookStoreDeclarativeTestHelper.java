package ch07;

public class BookStoreDeclarativeTestHelper {

	public static Book Bookオブジェクトの生成_MarinFowlerのRefactoring() {
		return new Book() {
			{
				title = "Refactoring";
				price = 4500;
				author = new Author() {
					{
						firstName = "Martin";
						lastName = "Fowler";
					}
				};
			}
		};
	}
}
