package ch04;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class AssertionFailTest {

	AssertionFail sut;
	
	@Before
	public void setUp() throws Exception {
		sut = new AssertionFail();
	}
	
	@Ignore("動作確認するときは@Ignoreをコメントアウトしてください")
	@Test
	public void test() {
		fail("TODO テストコードを実装する");
	}

	@Test(expected = IllegalStateException.class)
	public void timeoutがtrueの時にロジックが実行されないこと() {
		
		Runnable logic = new Runnable() {
			public void run() {
				fail("runが呼ばれてしまった");
			}
		};
		
		sut.timeOut = true;
		sut.invoke(logic);
	}
}
