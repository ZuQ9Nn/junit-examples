package ch04;

import static ch04.IsDate.dateOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;

@Ignore("動作確認するときは@Ignoreをコメントアウトしてください")
public class IsDateTest {

	@Test
	public void 日付の比較() throws Exception{
		Date date = new Date();
		assertThat(date, is(dateOf(2011, 2, 10)));
	}

	@Test
	public void nullとの比較() throws Exception {
		assertThat(null, is(dateOf(2011, 2, 10)));
	}
}
