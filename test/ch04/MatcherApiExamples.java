package ch04;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MatcherApiExamples {

	@Test
	public void CoreMatchers_is() throws Exception {
		String actual = "Hello" + " " + "World";
		String expected = "Hello World";
		assertThat(actual, is(expected));
	}
	
	@Test
	public void CoreMatcher_nullValue() throws Exception {
		String actual = null;
		assertThat(actual, is(nullValue()));
	}
	
	@Test
	public void CoreMathers_not() throws Exception {
		int actual = 100;
		assertThat(actual, is(not(0)));
	}
	
	@Test
	public void CoreMatchers_notNullValue() throws Exception {
		String actaul = "Hello";
		assertThat(actaul, is(notNullValue()));
		assertThat(actaul, is(not(nullValue())));
	}
	
	@Test
	public void CoreMatchers_sameInstance() throws Exception {
		Foo actual = new Foo();
		Foo expected = actual;
		assertThat(actual, is(sameInstance(expected)));
	}
	
	@Test
	public void CoreMatchers_instanceOf() throws Exception {
		Foo actual = new Foo();
		assertThat(actual, is(instanceOf(Serializable.class)));
	}
	
	@Test
	public void JunitMatchers_hasItem() throws Exception {
		Foo sut = new Foo();
		List<String> actual = sut.getList();
		assertThat(actual, is(hasItem("World")));
	}
	
	@Test
	public void JunitMatchers_hasItems() throws Exception {
		Foo sut = new Foo();
		List<String> actual = sut.getList();
		assertThat(actual, is(hasItems("Hello", "World")));
	}
	
	@SuppressWarnings("serial")
	static class Foo implements Serializable {
		List<String> getList() {
			List<String> list = new ArrayList<String>();
			list.add("Hello");
			list.add("Junit");
			list.add("World");
			return list;
		}
	}
}
