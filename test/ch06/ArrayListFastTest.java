package ch06;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ArrayListFastTest {
	
	private ArrayList<String> sut;
	
	@Before
	public void setUp() throws Exception {
		sut = new ArrayList<String>();
	}
	
	@Test
	public void listに1件追加してある場合は_sizeは1を返す() throws Exception {
		sut.add("A");
		int actual = sut.size();
		assertThat(actual, is(1));
	}
	
	@Test
	public void listに2件つかしたある場合は_sizeは2を返す() throws Exception {
		sut.add("A");
		sut.add("B");
		int actual = sut.size();
		assertThat(actual, is(2));
	}

}
