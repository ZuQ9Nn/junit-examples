package ch08;

import org.junit.Test;
import org.junit.experimental.theories.DataPoint;

public class ParameterizedTest {

	@DataPoint
	public static int INT_PARAM_1 = 3;
	@DataPoint
	public static int INT_PARAM_2 = 4;
	
	public ParameterizedTest() {
		System.out.println("初期化");
	}
	@Test
	public void 引数を持つテストメソッド(int param) throws Exception {
		System.out.println("引数を持つテストメソッド(" + param + ")");
	}

}
