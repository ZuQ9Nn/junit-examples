package ch08;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeThat;

import org.junit.Test;

public class AssmeTest {

	@Test
	public void assume() throws Exception {
		assumeThat(1, is(0));
		fail("この行は実行されない");
	}

}
