package ch08;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeTrue;

import org.junit.Test;

public class WIndowsOnlyTest {

	@Test
	public void windows環境では改行rn() throws Exception {
		assumeTrue(System.getProperty("os.name").contains("Windows"));
		assertThat(System.getProperty("line.separator"), is("\r\n"));
	}

}
