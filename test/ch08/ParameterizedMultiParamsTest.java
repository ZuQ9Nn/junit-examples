package ch08;

import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theory;

public class ParameterizedMultiParamsTest {

	@DataPoint
	public static int INT_PARAM_1 = 3;
	@DataPoint
	public static int INT_PARAM_2 = 4;
	@DataPoint
	public static String STRING_PARAM_1 = "Hello";
	@DataPoint
	public static String String_PARAM_2 = "World";
	
	@Theory
	public void テストメソッド(int intParam, String strParam) throws Exception {
		System.out.println("テストメソット(" + intParam + "," + strParam +")");
	}

}
