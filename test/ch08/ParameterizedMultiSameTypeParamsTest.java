package ch08;

import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theory;

public class ParameterizedMultiSameTypeParamsTest {

	@DataPoint
	public static int INT_PARAM_1 = 3;
	@DataPoint
	public static int INT_PARAM_2 = 4;
	
	@Theory
	public void テストメソッド(int x, int y) throws Exception {
		System.out.println("テストメソッド(" + x + "," + y +")");
	}

}
