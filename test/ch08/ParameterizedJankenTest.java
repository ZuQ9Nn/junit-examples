package ch08;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;

import ch08.Janken.Hand;
import ch08.Janken.Result;

@RunWith(Theories.class)
public class ParameterizedJankenTest {

	@Test
	public void test() {
		fail("Not yet implemented");
	}

	static class Fixture {
		final Hand h1;
		final Hand h2;
		final Result expected;
		
		public Fixture(Hand h1, Hand h2, Result expected) {
			this.h1 = h1;
			this.h2 = h2;
			this.expected = expected;
		}
		
		@Override
		public String toString() {
			return String.format("%s vs %s expected %s", h1, h2, expected);
		}
		
	}
}
