package ch06;

public class User {

	private String name = "nobody";
	private boolean adimin = false;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isAdimin() {
		return adimin;
	}
	public void setAdimin(boolean adimin) {
		this.adimin = adimin;
	}
}
