package ch08;

public class Members {

	public static boolean canRegister(int age) {
		return 18 <= age;
	}
	
	public static boolean isSecialMember(int age, boolean isRegisterMailMagazine, int usePastMonth) {
		if(age < 20) return false;
		if(!isRegisterMailMagazine) return false;
		if(usePastMonth < 1) return false;
		return true;
	}
	
	public static boolean canReister(Age age) {
		return 18 <= age.value;
	}
}
